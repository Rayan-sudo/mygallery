import React from 'react';
import classes from '../about/about.module.css';
import image8 from '../../images/image08.jpg';
import image9 from '../../images/image09.jpg';
class About extends React.Component {
    state={
        quote:"Creativity is contagious, pass it on.",
        author:"Albert Einstein",
        description:"We’re intrigued by the question of where creativity comes from. We do believe that one creative act or work has a tendency to inspire others. \nThus, we’d like you to take part in our next call for entries, which is an experiment to see just how far the seed of our creative call will spread. We all know the concept of a selfie. But what about a shelfie? Urban Dictionary defines a shelfie as “A picture or portrait of your bookshelf.\n Showcasing literature IN ALL ITS GLORY! … Not to be confused with selfie .” Literature is one way to do it. But as we have seen, creativity comes in many forms."
    }
    render(){
        return(
        <div className={classes.all}>
            <div className={classes.row1}>
                <div className={classes.col1}>
                    <div className={classes.content}>
                    <p  className={classes.quote}>{this.state.quote}</p>
                    <p  className={classes.author}>{this.state.author}</p>

                    </div>
                </div>
                <div className={classes.col2} style={{backgroundImage: `url(${image9})`}}>
                </div>
            </div>
            <div className={classes.row2}>
            <div className={classes.col3} style={{backgroundImage: `url(${image8})`}}>
                </div>
                <div className={classes.col4}>
                <div className={classes.content_}>
                 <p  className={classes.description}> {this.state.description}</p>
                 </div>
                </div>
            </div>
        </div>
            )
    }
}
export default About;