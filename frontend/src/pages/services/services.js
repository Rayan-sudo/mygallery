import React, { Component } from 'react';
import classes from './services.module.css'
import Contact_us from '../contact us/contact_us';

class Services extends Component{
    state={
        data:["Painting","Drawing", "sculpture"]
    }

  listItems = this.state.data.map((data) =>
  <li className={classes.item}>{data}</li>
);

    render(){
        return(
            <div className={classes.row}>
                <div className={classes.services}><p  className={classes.services_text}>SERVICES</p></div>
                <div className={classes.col1}> 
                <p className={classes.ligne1}>WE OFFER FOR YOU:</p>
                <ol>{this.listItems}</ol>
                <p className={classes.ligne2}> if you want to order an art please</p>
                <a href="/Contact_us">contact us</a>
                </div>
               

            </div>
        );
    }
}
export default Services;