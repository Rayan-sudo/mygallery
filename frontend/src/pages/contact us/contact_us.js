import React from 'react';
import classes from '../contact us/contact_us.module.css'

class Contact_us extends React.Component {
    constructor(props) {
        super(props);
        this.state = {date: '',
                      name:'',
                    phone:'',
                description:'',
            email:''};
    
        this.setdate = this.setdate.bind(this);
        this.setname = this.setname.bind(this);
        this.setemail = this.setemail.bind(this);
        this.setphone = this.setphone.bind(this);
        this.setdescription = this.setdescription.bind(this);
    }
    setdate(event) {
        this.setState({date:event.target.value});
        console.log(this.state);
      }
      setname(event) {
        this.setState({name:event.target.value});
        console.log(this.state);
      }
      setemail(event) {
        this.setState({email:event.target.value});
        console.log(this.state);
      }
      setphone(event) {
        this.setState({phone:event.target.value});
        console.log(this.state);
      }
      setdescription(event) {
        this.setState({description:event.target.value});
        console.log(this.state);
      }
 render(){
     return (
        <div className={classes.row}>
       <div className={classes.col1}>
        <input type="text" placeholder="Event date" name="date" value={this.state.value} onChange={this.setdate}></input>
        <input type="text" placeholder="your name" name="name" value={this.state.value} onChange={this.setname}></input>
        <input type="text" placeholder="Email" name="email" value={this.state.value} onChange={this.setemail}></input>
        <input type="text" placeholder="Phone number" name="phone" value={this.state.value} onChange={this.setphone}></input>
        <input type="text" placeholder="your description" name="description" className={classes.massage} value={this.state.value} onChange={this.setdescription}></input>
        <button ><b>Make an appointement</b></button>
     </div>
       <div className={classes.col2}>
           <p className={classes.title}>Book your Art</p>
           <p className={classes.description}>To ordre an art please share as many details about the 
               desined painting as possible,<br></br> we will be giving you any 
               feedback after i received your massage!
           </p>
       </div>
        </div>
        );
 }
}
export default Contact_us;