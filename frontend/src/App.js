import React from 'react';
import logo from './logo.svg';
import { Route, Switch } from 'react-router-dom';
import Contact_us from './pages/contact us/contact_us.js';
import Services from './pages/services/services.js'
import Header from './components/header/header.js'
import Home from './pages/home/home.js';
import About from './pages/about/about.js'
import Footer from './components/footer/footer.js';
import Navbar from './components/navbar/navbar.js';
function App() {
  return (
    <div>
       <Header/>
       <Navbar/>
    
    <div className="app-content">
    <Switch>
      <Route path="/About">
        <About/>
      </Route>
      
      <Route path="/Gallery">
      </Route>

      <Route path="/" exact component={Home}/>
      
      <Route path="/Contact_us" exact>
       <Contact_us />
      </Route>
      <Route path="/Services" exact>
       <Services/>
      </Route>
    
    </Switch>
    <Footer/>
    
  </div>

  </div>
  );
}

export default App;
