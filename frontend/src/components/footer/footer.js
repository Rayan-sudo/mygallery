import React from 'react';
import classes from './footer.module.css';
import Blogs from '../blogs/blogs.js';
import imagepost1 from '../../images/image01.jpg';
import imagepost2 from '../../images/image02.jpg';
import imagepost3 from '../../images/image03.jpg';
class Footer extends React.Component {
    state = {
        items: [
         
          {
            description: 'somthings description of blogs, writen here , youpiii lal lalaa laa',
            date: '11-2-2020',
            image: imagepost2,
            title: 'title2',
          },
          {
            description: 'somthings description of blogs, writen here , youpiii lal lalaa laa',
            date: '11-2-2020',
            image: imagepost3,
            title: 'title3',
          },
          {
            description: 'somthings description of blogs, writen here , youpiii lal lalaa laa',
            date: '11-2-2020',
            image: imagepost1,
            title: 'title1',
          },
        ]
    };
 render(){
  
     return (
         <div>
        <hr className={classes.line}></hr>
        <div className={classes.row}>
          <Blogs items={this.state.items}/>
        </div>
        </div>
        );
 }
}
export default Footer;