import React from "react";
import { Link } from "react-router-dom";
import classes from "./navbar.module.css";


const Navbar = (props) => {
  return (
    <nav  style={{ backgroundColor: "#F5F5F5" }} className={classes.navbar}>
      <ul >
      <li className="nav-item ">
      <Link className={classes.home} to="/" >
        Home
      </Link>
      </li>
        <li className="nav-item ">
          <Link  className={classes.about} to="/About">
            About
          </Link>
        </li>

        <li className="nav-item ">
          <Link  className={classes.gallery} to="/Gallery">
          Gallery
          </Link>
        </li>

        <li className="nav-item ">
          <Link  className={classes.services} to="/Services">
          Services
          </Link>
        </li>

        <li className="nav-item ">
          <Link  className={classes.contact} to="/Contact_us">
            Contact us
          </Link>
        </li>
      </ul>
    </nav>
  );
};

export default Navbar;
