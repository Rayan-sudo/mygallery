import React from 'react';
import classes from './blogs.module.css';

class Blogs extends React.Component {

    render() {
        const items = this.props.items;
        const theItems= items.map((item)=>{
            return( <div>
                <div className={classes.AllPost}>
                <div className={classes.ListItemsPost}>
                    <img className={classes.imagePost} src={item.image} alt="Blogs"/>
                    <p className={classes.namePost} >{item.title}</p>
                    <p className={classes.slugPost} >{item.slug}</p>
                    <p className={classes.descriptionPost} >{item.description}</p>
                    <p className={classes.datePost} >{item.date}</p>
                </div>
                </div>
                 </div>)
        })

        return (
            <div className="container">
            {theItems}
            </div>
        );
    }
   
}
export default Blogs;
