<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Art_info extends Model
{
    protected $table = 'art_info';
    protected $fillable = ['name','description','size','type','image'];
}
