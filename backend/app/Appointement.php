<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointement extends Model
{
    protected $table = 'appointement';
    protected $fillable = [ 'date','name','email' ,'description',"phone"];
}
