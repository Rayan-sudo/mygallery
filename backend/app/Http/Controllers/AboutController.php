<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\About;
class AboutController extends Controller
{
    /**
         * @return mixed
         */
        /**
         * To show all About page in database  
         */
        public function index()
        {
            $about = About::all()->toArray();
    
            return response()->json([
                'success' => true,
                'data' => $about
            ]);
        }
        /**
         * @param $id
         * @return \Illuminate\Http\JsonResponse
         */
        /**
         * to show one  of About page from database by ID
         */
        public function show($id)
        {
            $about = About::find($id);
    
            if (!$about) {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, About with id ' . $id . ' cannot be found.'
                ], 400);
            }
    
    
            return response()->json([
                'success' => true,
                'data' => $about
            ]);
        }
    
        /**
         * @param Request $request
         * @return \Illuminate\Http\JsonResponse
         * @throws \Illuminate\Validation\ValidationException
         */
        /**
         *Create Database for About page
         */
        public function store(Request $request)
        {
            $this->validate($request, [
                'quote' => 'required',
                'description' => 'required',
        
            ]);
    
            $about = new About();
            $about->quote = $request->quote;
            $about->description = $request->description;
           
    
            if ($about->save())
                return response()->json([
                    'success' => true,
                    'data' => $about
                ]);
            else
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, about could not be added.'
                ], 500);
        }
    
    
        /**
         * @param Request $request
         * @param $id
         * @return \Illuminate\Http\JsonResponse
         */
        /**
         * Update database on About page by ID 
         */
        public function update(Request $request, $id)
        {
            $about = About::find($id);
    
            if (!$about) {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, about with id ' . $id . ' cannot be found.'
                ], 400);
            }
    
            $updated = $about->fill($request->all())->save();
    
            if ($updated) {
                return response()->json([
                    'success' => true,
                    'data' => $about
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, about could not be updated.'
                ], 500);
            }
        }
    
        /**
         * @param $id
         * @return \Illuminate\Http\JsonResponse
         */
        /**
         * to delete database from About page by ID
         */
        public function destroy($id)
        {
            $about = about::find($id);
    
            if (!$about) {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, about with id ' . $id . ' cannot be found.'
                ], 400);
            }
    
            if ($about->delete()) {
                return response()->json([
                    'success' => true
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'about could not be deleted.'
                ], 500);
            }
        }
}
