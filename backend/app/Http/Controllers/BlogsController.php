<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;

class BlogsController extends Controller{
        /**
         * @return mixed
         */
        /**
         * To show all Blogs page in database  
         */
        public function index()
        {
            $blogs = Blog::all()->toArray();
    
            return response()->json([
                'success' => true,
                'data' => $blogs
            ]);
        }
        /**
         * @param $id
         * @return \Illuminate\Http\JsonResponse
         */
        /**
         * to show one  of Blogs page from database by ID
         */
        public function show($id)
        {
            $blog = Blog::find($id);
    
            if (!$blog) {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, blogs with id ' . $id . ' cannot be found.'
                ], 400);
            }
    
    
            return response()->json([
                'success' => true,
                'data' => $blog
            ]);
        }
    
        /**
         * @param Request $request
         * @return \Illuminate\Http\JsonResponse
         * @throws \Illuminate\Validation\ValidationException
         */
        /**
         *Create Database for Blogs page
         */
        public function store(Request $request)
        {
            $this->validate($request, [
                'title' => 'required',
                'image' => 'required',
                'description' => 'required',
                'date' => 'required',
                'slug' => 'required',
    
            ]);
    
            $blogs = new Blog();
            $blogs->title = $request->title;
            $blogs->image = $request->image;
            $blogs->description = $request->description;
            $blogs->date = $request->date;
            $blogs->slug = $request->slug;
    
    
            if ($blogs->save())
                return response()->json([
                    'success' => true,
                    'data' => $blogs
                ]);
            else
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, blog could not be added.'
                ], 500);
        }
    
    
        /**
         * @param Request $request
         * @param $id
         * @return \Illuminate\Http\JsonResponse
         */
        /**
         * Update database on Blogs page by ID 
         */
        public function update(Request $request, $id)
        {
            $blog = Blog::find($id);
    
            if (!$blog) {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, blog with id ' . $id . ' cannot be found.'
                ], 400);
            }
    
            $updated = $blog->fill($request->all())->save();
    
            if ($updated) {
                return response()->json([
                    'success' => true,
                    'data' => $blog
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, blog could not be updated.'
                ], 500);
            }
        }
    
        /**
         * @param $id
         * @return \Illuminate\Http\JsonResponse
         */
        /**
         * to delete database from About page by ID
         */
        public function destroy($id)
        {
            $blog = Blog::find($id);
    
            if (!$blog) {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, blog with id ' . $id . ' cannot be found.'
                ], 400);
            }
    
            if ($blog->delete()) {
                return response()->json([
                    'success' => true
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'blog could not be deleted.'
                ], 500);
            }
        }
    }
    