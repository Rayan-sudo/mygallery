<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Appointement;
class Appointementcontroller extends Controller
{
     /**
         * @return mixed
         */
        /**
         * To show all appointments page in database  
         */
        public function index()
        {

            $appointements = Appointement::all()->toArray();
    
            return response()->json([
                'success' => true,
                'data' => $appointements
            ]);
        }
      

        /**
         * @param $id
         * @return \Illuminate\Http\JsonResponse
         */
        /**
         * to show one  of appointments page from database by ID
         */
        public function show($id)
        {
            $appointement = Appointement::find($id);
    
            if (!$appointement) {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, appointments with id ' . $id . ' cannot be found.'
                ], 400);
            }
    
    
            return response()->json([
                'success' => true,
                'data' => $appointement
            ]);
        }
    
        /**
         * @param Request $request
         * @return \Illuminate\Http\JsonResponse
         * @throws \Illuminate\Validation\ValidationException
         */
        /**
         *Create Database for appointments page
         */
        public function store(Request $request)
        {
            $this->validate($request, [
                'title' => 'required',
                'image' => 'required',
                'description' => 'required',
                'date' => 'required',
                'slug' => 'required',
    
            ]);
    
            $appointement = new Appointement();
            $appointement->date = $request->date;
            $appointement->name = $request->name;
            $appointement->description = $request->description;
            $appointement->phone = $request->phone;
           
    
            if ($appointement->save())
                return response()->json([
                    'success' => true,
                    'data' => $appointement
                ]);
            else
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, Appointement could not be added.'
                ], 500);
        }
    
    
        /**
         * @param Request $request
         * @param $id
         * @return \Illuminate\Http\JsonResponse
         */
        /**
         * Update database on appointments page by ID 
         */
        public function update(Request $request, $id)
        {

            $appointment = Appointment::find($id);
    
            if (!$appointment) {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, Appointment with id ' . $id . ' cannot be found.'
                ], 400);
            }
    
            $updated = $appointment->fill($request->all())->save();
    
            if ($updated) {
                return response()->json([
                    'success' => true,
                    'data' => $appointment
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, Appointment could not be updated.'
                ], 500);
            }
        }
    
        /**
         * @param $id
         * @return \Illuminate\Http\JsonResponse
         */
        /**
         * to delete database from About page by ID
         */
        public function destroy($id)
        {
            $appointment = Appointment::find($id);
    
            if (!$appointement) {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, Appointement with id ' . $id . ' cannot be found.'
                ], 400);
            }
    
            if ($appointement->delete()) {
                return response()->json([
                    'success' => true
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Appointement could not be deleted.'
                ], 500);
            }
        }
}
