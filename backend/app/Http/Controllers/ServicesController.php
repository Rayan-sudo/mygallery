<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
class ServicesController extends Controller
{
        /**
         * @return mixed
         */
        /**
         * To show all services page in database  
         */
        public function index()
        {
            $services = Service::all()->toArray();
    
            return response()->json([
                'success' => true,
                'data' => $services
            ]);
        }
        /**
         * @param $id
         * @return \Illuminate\Http\JsonResponse
         */
        /**
         * to show one  of services page from database by ID
         */
        public function show($id)
        {
            $service = Service::find($id);
    
            if (!$service) {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, services with id ' . $id . ' cannot be found.'
                ], 400);
            }
    
    
            return response()->json([
                'success' => true,
                'data' => $service
            ]);
        }
    
        /**
         * @param Request $request
         * @return \Illuminate\Http\JsonResponse
         * @throws \Illuminate\Validation\ValidationException
         */
        /**
         *Create Database for services page
         */
        public function store(Request $request)
        {
            $this->validate($request, [
                'name' => 'required',
            ]);
    
            $service = new service();
            $service->name = $request->name;
    
    
    
            if ($service->save())
                return response()->json([
                    'success' => true,
                    'data' => $service
                ]);
            else
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, service could not be added.'
                ], 500);
        }
    
    
        /**
         * @param Request $request
         * @param $id
         * @return \Illuminate\Http\JsonResponse
         */
        /**
         * Update database on services page by ID 
         */
        public function update(Request $request, $id)
        {
            $service = Service::find($id);
    
            if (!$service) {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, service with id ' . $id . ' cannot be found.'
                ], 400);
            }
    
            $updated = $service->fill($request->all())->save();
    
            if ($updated) {
                return response()->json([
                    'success' => true,
                    'data' => $service
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, service could not be updated.'
                ], 500);
            }
        }
    
        /**
         * @param $id
         * @return \Illuminate\Http\JsonResponse
         */
        /**
         * to delete database from About page by ID
         */
        public function destroy($id)
        {
            $service = Service::find($id);
    
            if (!$service) {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, service with id ' . $id . ' cannot be found.'
                ], 400);
            }
    
            if ($service->delete()) {
                return response()->json([
                    'success' => true
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'service could not be deleted.'
                ], 500);
            }
        }
}
