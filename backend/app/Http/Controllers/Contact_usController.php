<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact_us;
class Contact_usController extends Controller
{
        /**
         * @return mixed
         */
        /**
         * To show all contact_uss page in database  
         */
        public function index()
        {
            $contact_us = Contact_us::all()->toArray();
    
            return response()->json([
                'success' => true,
                'data' => $contact_us
            ]);
        }
        /**
         * @param $id
         * @return \Illuminate\Http\JsonResponse
         */
        /**
         * to show one  of contact_uss page from database by ID
         */
        public function show($id)
        {
            $contact_us = Contact_us::find($id);
    
            if (!$contact_us) {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, contact_us with id ' . $id . ' cannot be found.'
                ], 400);
            }
    
    
            return response()->json([
                'success' => true,
                'data' => $contact_us
            ]);
        }
    
        /**
         * @param Request $request
         * @return \Illuminate\Http\JsonResponse
         * @throws \Illuminate\Validation\ValidationException
         */
        /**
         *Create Database for contact_uss page
         */
        public function store(Request $request)
        {
            $this->validate($request, [
                'phone' => 'required',
                'insta' => 'required',
                'fb' => 'required',
    
            ]);
    
            $contact_us = new contact_us();
            $contact_us->phone = $request->phone;
            $contact_us->fb = $request->fb;
            $contact_us->insta = $request->insta;
           
    
            if ($contact_us->save())
                return response()->json([
                    'success' => true,
                    'data' => $contact_us
                ]);
            else
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, contact_us could not be added.'
                ], 500);
        }
    
    
        /**
         * @param Request $request
         * @param $id
         * @return \Illuminate\Http\JsonResponse
         */
        /**
         * Update database on contact_uss page by ID 
         */
        public function update(Request $request, $id)
        {
            $contact_us = Contact_us::find($id);
    
            if (!$contact_us) {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, contact_us with id ' . $id . ' cannot be found.'
                ], 400);
            }
    
            $updated = $contact_us->fill($request->all())->save();
    
            if ($updated) {
                return response()->json([
                    'success' => true,
                    'data' => $contact_us
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, contact_us could not be updated.'
                ], 500);
            }
        }
    
        /**
         * @param $id
         * @return \Illuminate\Http\JsonResponse
         */
        /**
         * to delete database from contact_us page by ID
         */
        public function destroy($id)
        {
            $contact_us = Contact_us::find($id);
    
            if (!$contact_us) {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, contact_us with id ' . $id . ' cannot be found.'
                ], 400);
            }
    
            if ($contact_us->delete()) {
                return response()->json([
                    'success' => true
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'contact_us could not be deleted.'
                ], 500);
            }
        }
}
