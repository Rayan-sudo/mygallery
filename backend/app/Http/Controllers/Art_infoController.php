<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Art_info;
class Art_infoController extends Controller
{
    /**
         * @return mixed
         */
        /**
         * To show all art_info page in database  
         */
        public function index()
        {
            $art_info = Art_info::all()->toArray();
    
            return response()->json([
                'success' => true,
                'data' => $art_info
            ]);
        }
        /**
         * @param $id
         * @return \Illuminate\Http\JsonResponse
         */
        /**
         * to show one  of art_info page from database by ID
         */
        public function show($id)
        {
            $art_info = Art_info::find($id);
    
            if (!$art_info) {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, art_info with id ' . $id . ' cannot be found.'
                ], 400);
            }
    
    
            return response()->json([
                'success' => true,
                'data' => $art_info
            ]);
        }
    
        /**
         * @param Request $request
         * @return \Illuminate\Http\JsonResponse
         * @throws \Illuminate\Validation\ValidationException
         */
        /**
         *Create Database for art_info page
         */
        public function store(Request $request)
        {
            $this->validate($request, [
                'name' => 'required',
                'description' => 'required',
                'size' => 'required',
                'type' => 'required',
                'image'=>'required',
            ]);
    
            $art_info = new Art_info();
            $art_info->name = $request->name;
            $art_info->description = $request->description;
            $art_info->type = $request->type;
            $art_info->size = $request->size;
            $art_info->image = $request->image;
    
    
            if ($art_info->save())
                return response()->json([
                    'success' => true,
                    'data' => $art_info
                ]);
            else
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, art_info could not be added.'
                ], 500);
        }
    
    
        /**
         * @param Request $request
         * @param $id
         * @return \Illuminate\Http\JsonResponse
         */
        /**
         * Update database on art_info page by ID 
         */
        public function update(Request $request, $id)
        {
            $art_info = Art_info::find($id);
    
            if (!$art_info) {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, art_info with id ' . $id . ' cannot be found.'
                ], 400);
            }
    
            $updated = $art_info->fill($request->all())->save();
    
            if ($updated) {
                return response()->json([
                    'success' => true,
                    'data' => $art_info
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, art_info could not be updated.'
                ], 500);
            }
        }
    
        /**
         * @param $id
         * @return \Illuminate\Http\JsonResponse
         */
        /**
         * to delete database from About page by ID
         */
        public function destroy($id)
        {
            $art_info = Art_info::find($id);
    
            if (!$art_info) {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, art with id ' . $id . ' cannot be found.'
                ], 400);
            }
    
            if ($art_info->delete()) {
                return response()->json([
                    'success' => true
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'art_info could not be deleted.'
                ], 500);
            }
        }
}
