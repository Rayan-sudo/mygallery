<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/blogs', 'BlogsController@index');
Route::get('/blogs/{id}', 'BlogsController@show');
Route::post('/blogs/create', 'BlogsController@store' );
Route::put('/blogs/update/{id}', 'BlogsController@update');
Route::delete('/blogs/delete/{id}', 'BlogsController@destroy');


Route::get('/about', 'AboutController@index');
Route::get('/about/{id}', 'AboutController@show');
Route::post( '/about/create', 'AboutController@store' );
Route::put('/about/update/{id}', 'AboutController@update');
Route::delete('/about/delete/{id}', 'AboutController@destroy');



Route::get('/appointement', 'AppointementController@index');
Route::get('/Appointement/{id}', 'AppointementController@show');
Route::post( '/Appointement/create', 'AppointementController@store' );
Route::put('/Appointement/update/{id}', 'AppointementController@update');
Route::delete('/Appointement/delete/{id}', 'AppointementController@destroy');

Route::get('/art_info ', 'art_infoController@index');
Route::get('/art_info/{id}', 'art_infoController@show');
Route::post( '/art_info/create', 'Art_infoController@store' );
Route::put('/art_info/update/{id}', 'art_infoController@update');
Route::delete('/art_info/delete/{id}', 'art_infoController@destroy');

Route::get('/contact_us ', 'contact_usController@index');
Route::get('/contact_us/{id}', 'contact_usController@show');
Route::post( '/contact_us/create', 'Contact_usController@store' );
Route::put('/contact_us/update/{id}', 'contact_usController@update');
Route::delete('/contact_us/delete/{id}', 'contact_usController@destroy');


Route::get('/service', 'servicesController@index');
Route::get('/service/{id}', 'servicesController@show');
Route::post( '/service/create', 'ServicesController@store' );
Route::put('/service/update/{id}', 'servicesController@update');
Route::delete('/service/delete/{id}', 'servicesController@destroy');
